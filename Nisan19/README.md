##Devops

Case1 1.soru için Nisan19 klasörü içinde oluşturulan Case1_1 formatlanarak başlanacaktır. Aynı şekilde 2.soru için de Case2_2 formatında ilerlenmiş, ekran görüntüleri ayrı bir klasörde yedeklenmiştir.
Konfigürasyonlarda ihtiyaç duyulan diğer dosyalar (örneğin index.html, nginx.default.conf) Case1_* ana dizinlerinde tutulacaktır ayrıca belirtilmediği sürece.
Setup centos7 klasöründe koştuğum her komutu ve çıktılarını; bunları destekleyen ekran görüntülerini ekledim.
Hypervisor olarak Virtualbox, imaj olarak CentOS-7-x86_64-DVD-1511.iso kurulum diskini kullandım.


Case1 2.soruda controller olarak centos7 35.236.109.188, worker olarak 34.94.169.249 centos7 sunucularını açtım.
root altında templates altında statik sayfanın yönlendirildiği index.html ve nginx.yemre.conf dosyasını oluşturdum
root altındaki install_nginx.yml ve install_docker.yml ile worker sunucuda playbook ile nginx ve docker kurulumlarını gerçekleştirdim.
DB kullanan uygulama olarak wordpress'i seçtim ve mysql'e bind ederek stack.yml ile dokcerize edip task.yml ile worker'a ilgili dosyaları taşıyıp, docker-compose up ve restart nginx ile web uygulamamı serve ettim.

Basitçe ansible-playbook ile (roles ve workshop'daki gibi groups varsa variable tanımlamadım), kendi oluşturduğum basit mimaride istenileni yakalamaya çalıştım. 


Kaynaklar:
https://hub.docker.com/_/wordpress?tab=description&page=1&ordering=last_updated
https://stackoverflow.com/questions/12431496/nginx-read-custom-header-from-upstream-server
https://docs.ansible.com/ansible/2.9/modules/copy_module.html#copy-module
https://www.youtube.com/watch?v=TWcFAi6rvlc
https://docs.ansible.com/ansible/2.9/user_guide/command_line_tools.html
